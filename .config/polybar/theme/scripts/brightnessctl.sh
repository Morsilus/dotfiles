#!/bin/bash

brightness=$(brightnessctl g)
max_brightness=$(brightnessctl m) 

brightness_percentage=$(( 100 * brightness / max_brightness ))

if [ $brightness_percentage -le 5 ]; then
    icon="󰃚"
elif [ $brightness_percentage -le 12 ]; then
    icon="󰃛"
elif [ $brightness_percentage -le 25 ]; then
    icon="󰃜"
elif [ $brightness_percentage -le 40 ]; then
    icon="󰃝"
elif [ $brightness_percentage -le 60 ]; then
    icon="󰃞"
elif [ $brightness_percentage -le 80 ]; then
    icon="󰃟"
else
    icon="󰃠"
fi

echo "$icon  $brightness_percentage %"
