#!/bin/bash

killall -q polybar

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

config="$HOME/.config/polybar/config_${HOSTNAME}.ini"

case $HOSTNAME in
	archlinux)
        polybar -q main -c $config &
        polybar -q left -c $config &
		;;
	Hopcyn)
        polybar -q main -c $config &
        polybar -q left -c $config &
		;;
	*)
        polybar -q main -c $config &
		;;
esac
