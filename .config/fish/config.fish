function fish_prompt
	powerline-shell --shell bare $status
end

test -s /home/robert/.nvm-fish/nvm.fish; and source /home/robert/.nvm-fish/nvm.fish

# Created by `pipx` on 2023-08-08 10:41:15
set PATH $PATH /home/robert/.local/bin

switch $hostname
    case 'R3mY'
        # Set DPI for Qt applications
        set -Ux QT_AUTO_SCREEN_SCALE_FACTOR 0
        set -Ux QT_SCALE_FACTOR 1.5
        set -Ux QT_SCREEN_SCALE_FACTORS 1.5
		# Set PATH for bash scripts
		set PATH $PATH /home/robert/repositories/work/bashscripts
    case 'Hopcyn'
		set PATH $PATH /home/robert/repositories/bashscripts
end

