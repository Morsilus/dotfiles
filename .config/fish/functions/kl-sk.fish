function kl-sk --wraps='setxkbmap -layout sk' --wraps='setxkbmap -layout us' --description 'alias kl-sk setxkbmap -layout sk'
  setxkbmap -layout sk $argv; 
end
