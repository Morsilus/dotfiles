function ddg --wraps='lynx https://www.duckduckgo.com/' --wraps='lynx https://lite.duckduckgo.com/lite/' --description 'alias ddg lynx https://lite.duckduckgo.com/lite/'
  lynx https://lite.duckduckgo.com/lite/ $argv
        
end
