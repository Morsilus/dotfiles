#!/bin/bash

# Get the current layout
current_layout=$(setxkbmap -query | grep layout | awk '{print $2}')

# Switch to the other layout
if [ "$current_layout" == "us" ]; then
    setxkbmap sk -variant qwerty
elif [ "$current_layout" == "sk" ]; then
    setxkbmap cz -variant qwerty
else
    setxkbmap us
fi

