" An example for a vimrc file.
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2019 Dec 17
"
" To use it, copy it to
"	       for Unix:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"	 for MS-Windows:  $VIM\_vimrc
"	      for Haiku:  ~/config/settings/vim/vimrc
"	    for OpenVMS:  sys$login:.vimrc

" When started as "evim", evim.vim will already have done these settings, bail
" out.
if v:progname =~? "evim"
  finish
endif

" Get the defaults that most users want.
source $VIMRUNTIME/defaults.vim

if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file (restore to previous version)
  if has('persistent_undo')
    set undofile	" keep an undo file (undo changes after closing)
  endif
endif

if &t_Co > 2 || has("gui_running")
  " Switch on highlighting the last used search pattern.
  set hlsearch
endif

" Put these in an autocmd group, so that we can delete them easily.
augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78
augroup END

" Add optional packages.
"
" The matchit plugin makes the % command work better, but it is not backwards
" compatible.
" The ! means the package won't be loaded right away but when plugins are
" loaded during initialization.
if has('syntax') && has('eval')
  packadd! matchit
endif

set encoding=utf-8
set exrc
set secure

let g:powerline_pycmd="py3"
set runtimepath^=~/.vim/bundle/vim-gencode-cpp

" * * * * * * * * * * * * * *

call plug#begin('~/.vim/plugged')

Plug 'Valloric/YouCompleteMe', { 'do': './install.py' }
Plug 'ternjs/tern_for_vim', { 'do': 'npm install' }
Plug 'flazz/vim-colorschemes'
Plug 'flazz/-colorschemes'

call plug#end()

" * * * * * * * * * * * * * *

python3 from powerline.vim import setup as powerline_setup
python3 powerline_setup()
python3 del powerline_setup
set laststatus=2
set encoding=utf-8
let g:Powerline_symbols = 'fancy'


" * * * * * * * * * * * * * *

set t_ut=""
set background=dark
colorscheme gruvbox

" * * * * * * * * * * * * * *

set clipboard=unnamedplus
set number relativenumber

" * * * * * * * * * * * * * *

let g:netrw_fastbrowse=0
let g:ycm_add_preview_to_completeopt = 0

" * * * * * * * * * * * * * *

set tabstop=4
set shiftwidth=4
set autoindent

" * * * * * * * * * * * * * *

autocmd BufNewFile,BufRead *.py source ~/.config/vim/python
autocmd BufNewFile,BufRead *.hs source ~/.config/vim/haskell
autocmd BufNewFile,BufRead *.php source ~/.config/vim/php

let g:time_tracker_location = '~/documents/vim/time_tracking.log'
let g:tt_max_inactivity = 300
function LogTimeTracked()
	if &filetype ==# 'netrw'
		return
	endif
	let l:log = expand(strftime(g:time_tracker_location))
	let l:file = expand("<afile>:p")
	let l:start = strftime("%D|%T", b:start_time)
	let l:stop_time = strftime("%s")
	if b:last_activity + g:tt_max_inactivity < l:stop_time
		let l:stop_time = b:last_activity + g:tt_max_inactivity
	endif
	let l:stop = strftime("%D|%T", l:stop_time)
	let l:time = strftime("%d|%T", l:stop_time - b:start_time)
	let l:line = [l:file . " " . l:start . " " . l:stop . " " . l:time]
	call writefile(l:line, l:log, "a")
endfunction
"augroup Tracker
"" * * * * * * * * * * * * * *
"	autocmd InsertLeave * let b:last_activity = strftime("%s")
"	autocmd BufReadPost * let b:start_time = strftime("%s")
"			let b:last_activity = strftime("%s")
"	autocmd BufWinLeave * :execute LogTimeTracked()
"augroup END
